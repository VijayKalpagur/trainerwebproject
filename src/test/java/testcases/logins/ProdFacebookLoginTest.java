package testcases.logins;

import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.trainerweb.qa.actions.CloseBrowserAction;
import com.trainerweb.qa.actions.FacebookAction;
import com.trainerweb.qa.helpers.Browserhelper;
import com.trainerweb.qa.testdata.LoginDataProvider;
import com.trainerweb.qa.utilities.Log;



public class ProdFacebookLoginTest {

	static WebDriver driver;
	
@BeforeClass
public void OpenBrowser() throws Exception {	
	PropertyConfigurator.configure("log4j.properties");
	driver = Browserhelper.openProdBrowser();}
	
@Test (dataProvider = "FacebookLogin" , dataProviderClass = LoginDataProvider.class)
public static void ProductionFacebookLogin(String Facebookid,String FacebookPswd) throws Exception{	
	FacebookAction.socialFacebookProdLogin(driver, Facebookid, FacebookPswd);
	//LogoutAction.UserLogOut(driver);
	}

@AfterClass
public void teardown() throws Exception{	
	CloseBrowserAction.Application(driver);}
}

