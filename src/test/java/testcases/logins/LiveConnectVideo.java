package testcases.logins;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.trainerweb.qa.actions.LiveConnectVideoAction;
import com.trainerweb.qa.actions.LoginAction;
import com.trainerweb.qa.actions.LogoutAction;
import com.trainerweb.qa.helpers.Browserhelper;
import com.trainerweb.qa.testdata.LoginDataProvider;


public class LiveConnectVideo {

	static WebDriver driver;
	
	@BeforeClass
	public void openBrowser() throws Exception {
        driver=Browserhelper.openBrowser();		
	}
	
	@Test (dataProvider = "VideoSessionAccountLogin" , dataProviderClass = LoginDataProvider.class)
	public void TrainerLogin(String Username ,String Password) throws Exception{
		
		LoginAction.TrainerLogin(driver, Username, Password);
		LiveConnectVideoAction.checkLiveConnectVideo(driver);}
	   // LogoutAction.UserLogOut(driver);}
	
	@AfterClass
	public void quitBrowser() {
		driver.quit();
	}
}
