package testcases.logins;

import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.trainerweb.qa.actions.CloseBrowserAction;
import com.trainerweb.qa.actions.DailySessionAction;
import com.trainerweb.qa.actions.LoginAction;
import com.trainerweb.qa.helpers.Browserhelper;
import com.trainerweb.qa.testdata.LoginDataProvider;
import com.trainerweb.qa.utilities.Log;


public class DailyWorkoutSessionTest {

	static WebDriver driver;
	
@BeforeClass
public void OpenBrowser() throws Exception {	
	PropertyConfigurator.configure("log4j.properties");
	driver = Browserhelper.openBrowser();}

@Test (dataProvider = "DailySessionsaccountLogin" , dataProviderClass = LoginDataProvider.class)
public void DailySession(String Username ,String Password) throws Exception{
	LoginAction.TrainerLogin(driver,Username,Password);
	DailySessionAction.DailySession(driver);
 //   LogoutAction.UserLogOut(driver);
	}

@AfterClass
public void teardown() throws Exception{
	CloseBrowserAction.Application(driver);}
}

