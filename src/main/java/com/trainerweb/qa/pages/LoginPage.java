package com.trainerweb.qa.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;


public class LoginPage {

	static WebDriver driver;
	
public LoginPage(WebDriver driver){
	
	PageFactory.initElements(driver, this);
}
	@FindBy(xpath = "//*[@id='username']")
	public WebElement TrainerUsername;

	//@FindBy(how=How.XPATH,using= "//*[@id='loginpassword']")
	@FindBy(how=How.XPATH, using="//*[@id='password']")
	public WebElement TrainerPassword;

	@FindBy(how=How.XPATH,using = "//*[@id=\'login-form-button\']")
	public WebElement TrainerLoginBtn;
	

}

