package com.trainerweb.qa.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class SessionsPaymentPages {

public SessionsPaymentPages(WebDriver driver){
  PageFactory.initElements(driver, this);}
	
//**************************** Session payment with Cards ***********************
	
@FindBy(how=How.XPATH,using = "//*[@id='price']")
public WebElement SessionPrice;

@FindBy(how=How.XPATH,using = "//*[@id='paymentDisable']/ul/li[2]/button")
public WebElement CreditorDebitCard;

@FindBy(how=How.XPATH,using = "//div[@id='card-element']//iframe[1]")
public WebElement EnterCardNumber;

@FindBy(how=How.XPATH,using = "//*[@id='card']/div/form/div[2]/button")
public WebElement ClickonPayButton;

@FindBy(how=How.XPATH,using = "//div[@class='col-sm-12']//div[@class='fb-summ-hd']//div[4]//a[@class='tr-pr-me btn btn-default btn-sm']")
public WebElement ClickonPreviewSessionButton;


//**************************** Session payment with Paypal ***********************

@FindBy(how=How.XPATH,using = "//*[@id='paymentDisable']/ul/li[3]/button")
public WebElement ClickonPayPalTab;

@FindBy(how=How.XPATH,using = "//*[@id=\"paypal\"]/div/div/button")
public WebElement ContinuetoPayPalButton;

@FindBy(how=How.XPATH,using = "//*[@id='loginSection']/div/div[2]/a")
public WebElement PayPalLoginButton;

@FindBy(how=How.XPATH,using = "//*[@id='email']")
public WebElement EnterPayPalEmailID;

@FindBy(how=How.XPATH,using = "//*[@id='btnNext']")
public WebElement ClickOnNextButton;

@FindBy(how=How.XPATH,using = "//*[@id='password']")
public WebElement EnterPayPalPassword;

@FindBy(how=How.XPATH,using = "//div[@class='actions']//button[@id='btnLogin']")
public WebElement ClickOnLoginButton;

@FindBy(how=How.XPATH,using = "//*[@id=\"payment-submit-btn\"]")
public WebElement ClickonconfirmButton;


}
