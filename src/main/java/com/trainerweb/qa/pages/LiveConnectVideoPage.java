package com.trainerweb.qa.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class LiveConnectVideoPage {
	static WebDriver driver;
	
	public LiveConnectVideoPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath="/html/body/div/nav/div[1]/div/div/div/div/div[3]/div[1]")
	public WebElement msg_NoLiveSessionsAvailable;
	
	@FindBy(how=How.XPATH, using="//*[@id=\"livenotification-content\"]/a/em")
	public WebElement btn_LiveSessionsNotificationsPopup;
	
	@FindBy(xpath="/html/body/div/nav/div[1]/div/div/div/div/div[3]/div[1]")
	public WebElement btn_LiveSession;

}
