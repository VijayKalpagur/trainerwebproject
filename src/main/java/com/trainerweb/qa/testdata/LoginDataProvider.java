package com.trainerweb.qa.testdata;

import org.testng.annotations.DataProvider;

public class LoginDataProvider {

@DataProvider(name = "UserLogin" )//New Registered User
	public static Object[][] loginDataFromUserLoginDataprovider(){
	return new Object[][]{{"sunilkumar10@mailintor.com", "password"}};
}
@DataProvider(name = "GmailLogin" )//Social Login
	public static Object[][] gmailDataprovider(){
	return new Object[][]{{"fitbase.mail1@gmail.com", "testing123$"}};
}
@DataProvider(name = "FacebookLogin" )//Social Login
	public static Object[][] FacebookDataprovider(){
	return new Object[][]{{"fitbase.mail1@gmail.com", "testing123$"}};
}	
@DataProvider(name = "ProdUserLogin" )//New Registered User
	public static Object[][] UserDataProvider(){
	return new Object[][]{{"srinivas.kona@veritis.com", "password"}};}	
	
@DataProvider(name = "VideoSessionAccountLogin" )//New Registered User
	public static Object[][] VideoSessionDataProvider(){
	return new Object[][]{{"sunilkumar14@mailinator.com", "password"}};}

@DataProvider(name = "SingleSessionsaccountLogin" )//New Registered User
public static Object[][] SingleSessionsaccountDataProvider(){
return new Object[][]{{"sunilkumar300@mailinator.com", "password"}};}	

@DataProvider(name = "DailySessionsaccountLogin" )//New Registered User
public static Object[][] DailySessionsaccountDataProvider(){
return new Object[][]{{"sunilkumar301@mailinator.com", "password"}};}

@DataProvider(name = "WeeklySessionsaccountLogin" )//New Registered User
public static Object[][] WeeklySessionsaccountDataProvider(){
return new Object[][]{{"sunilkumar302@mailinator.com", "password"}};}

@DataProvider(name = "MonthlySessionsaccountLogin" )//New Registered User
public static Object[][] MonthlySessionsaccountDataProvider(){
return new Object[][]{{"sunilkumar303@mailinator.com", "password"}};}

@DataProvider(name = "CustomSessionsaccountLogin" )//New Registered User
public static Object[][] CustomSessionsaccountDataProvider(){
return new Object[][]{{"sunilkumar304@mailinator.com", "password"}};}

@DataProvider(name = "UserPaymentLogin" )//New Registered User
public static Object[][] loginDataFromUserPaymentLoginDataprovider(){
return new Object[][]{{"sunilkumar22@mailinator.com", "password"}};}

@DataProvider(name = "UserPaymentPaypalLogin" )//New Registered User
public static Object[][] loginDataFromUserPaymentPaypalLoginDataprovider(){
return new Object[][]{{"sunilkumar26@mailinator.com", "password"}};}
}

