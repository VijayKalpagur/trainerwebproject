package com.trainerweb.qa.actions;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import com.trainerweb.qa.helpers.WindowsHandlehelper;
import com.trainerweb.qa.pages.SocialLoginPage;
import com.trainerweb.qa.utilities.Log;




public class GmailAction {

	static WebDriver driver;
	
public static void socialGmailLogin(WebDriver driver ,String Gmailid,String GmailPswd) throws Exception{		
		
		SocialLoginPage Gmail = new SocialLoginPage(driver);
		
		Gmail.ClickonGmailbutton.isDisplayed();
		Gmail.ClickonGmailbutton.isEnabled();
		Gmail.ClickonGmailbutton.click();
		Thread.sleep(2000);
		Log.info("Click action performed on clickongmailbutton.");
		
		WindowsHandlehelper.WindowsHandle(driver, 2);
		
		Gmail.GmailUserID.isDisplayed();
		Gmail.GmailUserID.clear();
		Gmail.GmailUserID.sendKeys(Gmailid);
		Thread.sleep(2000);
		Log.info("Entered valid Gmail ID" );
		
		Gmail.GmailNextButton.isEnabled();
		Gmail.GmailNextButton.click();	
		Thread.sleep(3000);
		Log.info("Click action performed on GmailNextButton.");
				
		Gmail.GmailUserPassword.clear();
		Gmail.GmailUserPassword.sendKeys(GmailPswd);
		Thread.sleep(2000);
		Log.info("Entered valid Gmail Password" );
			
		Gmail.GmailNextButtontoLogin.isDisplayed();
		Gmail.GmailNextButtontoLogin.isEnabled();
		Gmail.GmailNextButtontoLogin.click();	
		
		Thread.sleep(15000);
		
		WindowsHandlehelper.WindowsHandle(driver, 1);
		Log.info("Click action performed on GmailNextButton.");	
		
		Thread.sleep(7000);
		
	//	Assert.assertEquals(driver.getCurrentUrl(), "https://qa.fitbase.com/trainer/#/dashboard");
		Log.info("Succesfully vaidated dashboard page");		
		Thread.sleep(2000);	
		Log.info("Successfully validated Gmail Login functionality with valid inputs");
	}	
public static void socialGmailProdLogin(WebDriver driver ,String Gmailid,String GmailPswd) throws Exception{		
	
	SocialLoginPage Gmail = new SocialLoginPage(driver);
	
	Gmail.ClickonGmailbutton.isDisplayed();
	Gmail.ClickonGmailbutton.isEnabled();
	Gmail.ClickonGmailbutton.click();
	Thread.sleep(2000);
	Log.info("Click action performed on clickongmailbutton.");
	
	WindowsHandlehelper.WindowsHandle(driver, 2);
	
	Gmail.GmailUserID.isDisplayed();
	Gmail.GmailUserID.clear();
	Gmail.GmailUserID.sendKeys(Gmailid);
	Thread.sleep(2000);
	Log.info("Entered valid Gmail ID" );
	
	Gmail.GmailNextButton.isEnabled();
	Gmail.GmailNextButton.click();	
	Thread.sleep(3000);
	Log.info("Click action performed on GmailNextButton.");
			
	Gmail.GmailUserPassword.clear();
	Gmail.GmailUserPassword.sendKeys(GmailPswd);
	Thread.sleep(2000);
	Log.info("Entered valid Gmail Password" );
		
	Gmail.GmailNextButtontoLogin.isDisplayed();
	Gmail.GmailNextButtontoLogin.isEnabled();
	Gmail.GmailNextButtontoLogin.click();	
	
	Thread.sleep(15000);
	
	WindowsHandlehelper.WindowsHandle(driver, 1);
	Log.info("Click action performed on GmailNextButton.");	
	
	driver.findElement(By.xpath("/html/body/div[1]/div/div/div/div/div/a[2]")).click();
	
	Thread.sleep(7000);
	
		
	Assert.assertEquals(driver.getCurrentUrl(), "https://www.fitbase.com/trainer/#/dashboard");
	Log.info("Succesfully vaidated dashboard page");		
	Thread.sleep(2000);
	Log.info("Successfully validated Production Gmail Login functionality with valid inputs");
}		
}
