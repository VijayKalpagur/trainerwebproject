package com.trainerweb.qa.actions;

import java.util.Iterator;
import java.util.Set;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.trainerweb.qa.pages.LiveConnectVideoPage;
import com.trainerweb.qa.utilities.Log;



public class LiveConnectVideoAction {
	public static WebDriver driver;

public static void checkLiveConnectVideo(WebDriver driver) throws InterruptedException {
		LiveConnectVideoPage liveConnect = new LiveConnectVideoPage(driver);
		WebDriverWait wait = new WebDriverWait(driver, 80);

			
			if (liveConnect.msg_NoLiveSessionsAvailable.isDisplayed() == true) {
				wait.until(ExpectedConditions.elementToBeClickable(liveConnect.btn_LiveSession)).click();
				Log.info("Click action performed on active live button to check video...");
				Thread.sleep(18000);
				
			
			  Set<String> window = driver.getWindowHandles(); 
			  Iterator<String> it = window.iterator();
			  
			  String parentWindow = it.next(); 
			  String childWindow = it.next();
			  driver.switchTo().window(childWindow);
			  Log.info("Switching to live video sessions page..."); 
				
				Assert.assertEquals(driver.getTitle(), "Live");
				Log.info("Live video page is verified successfully...");
				
				driver.switchTo().alert().accept();

				driver.switchTo().window(parentWindow);
				Log.info("Switching back to parent widnow to logout...");
				 

			} else {
				Log.info("There are no live sessions available to check...");
			}
	} /*
		 * catch (Exception e) { } finally {
		 * driver.manage().timeouts().implicitlyWait(1000, TimeUnit.SECONDS); } }
		 */
}

