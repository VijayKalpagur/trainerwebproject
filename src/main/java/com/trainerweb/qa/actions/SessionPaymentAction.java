package com.trainerweb.qa.actions;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;

import com.trainerweb.qa.helpers.CreateScheduleshelper;
import com.trainerweb.qa.pages.CreateSingleSessionPage;
import com.trainerweb.qa.pages.SessionsPaymentPages;
import com.trainerweb.qa.utilities.Constant;
import com.trainerweb.qa.utilities.Log;


public class SessionPaymentAction {

	static WebDriver driver;
	
	public static void SessionPaymentwithCard(WebDriver driver) throws Exception{
		CreateSingleSessionPage singlesession = new CreateSingleSessionPage(driver);
		SessionsPaymentPages SessionPayment = new SessionsPaymentPages(driver);
		
		    singlesession.CreateScheduleButton.isDisplayed();
			singlesession.CreateScheduleButton.isEnabled();
			singlesession.CreateScheduleButton.click();
		    Thread.sleep(2000);
			Log.info("Click action performed on CreateScheduleButton.");
			
			singlesession.NameoftheWorkoutSession.isDisplayed();
			singlesession.NameoftheWorkoutSession.clear();
			singlesession.NameoftheWorkoutSession.sendKeys(CreateScheduleshelper.singleworkoutsession());
		    Thread.sleep(1000);
			Log.info("Entered name of the workoutsession");
			
			singlesession.TypeofWorkout.isDisplayed();
//			singlesession.TypeofWorkout.click();
//			singlesession.TypeofWorkout.sendKeys(CreateScheduleshelper.TypeofWorkout());
			Log.info("Session TypeofWorkout is selected");
			Thread.sleep(1000);
			
//			singlesession.ClickonGroupSize.click();
//			singlesession.GroupSize.sendKeys(CreateScheduleshelper.GroupSession());
//			Log.info("Group Session size is selected");
			
			singlesession.WorkoutActivity.isDisplayed();
			singlesession.WorkoutActivity.click();
			
			singlesession.WorkoutAcvtivitySelection.clear();
			singlesession.WorkoutAcvtivitySelection.sendKeys(CreateScheduleshelper.WorkoutActivity());
			Thread.sleep(3000);
			
			singlesession.ClickonWorkoutAcvtivity.click();
		    Thread.sleep(1000);
			Log.info("Session WorkoutActivity is selected");
			
			singlesession.TrainingLevel.isDisplayed();
			singlesession.TrainingLevel.click();
			singlesession.TrainingLevel.sendKeys(CreateScheduleshelper.TrainingLevel());
		    Thread.sleep(1000);
			Log.info("Session TrainingLevel is selected");
			
			singlesession.SessionDate.isDisplayed();
			singlesession.SessionDate.click();
			singlesession.SessionDate.sendKeys(CreateScheduleshelper.AddDatestocurrentSystemDate("mm/dd/yyyy",Constant.StartDate));
			Thread.sleep(2000);
			Log.info("Session Date is selected");
			
			singlesession.SessionTime.isDisplayed();
			singlesession.SessionTime.clear();
			singlesession.SessionTime.sendKeys(CreateScheduleshelper.randomTimeGenerate("hh:mm a"));
			Log.info("Session time is selected");
			
			singlesession.SessionDuration.isDisplayed();
//			singlesession.SessionDuration.click();
//			singlesession.SessionDuration.sendKeys(CreateScheduleshelper.Duration("Minutes"));
			Log.info("Session Duration is selected");
			Thread.sleep(3000);
			
			Actions action = new Actions(driver);
			action.sendKeys(Keys.PAGE_DOWN).build().perform();
			
			SessionPayment.SessionPrice.clear();
			Thread.sleep(1000);
			SessionPayment.SessionPrice.sendKeys("0");	
			
			Thread.sleep(1000);
			singlesession.PublishButton.isDisplayed();
			singlesession.PublishButton.isEnabled();
			singlesession.PublishButton.click();
			Log.info("Click action performed on Publish Button.");
		    Thread.sleep(15000);
			
			driver.findElement(By.xpath("/html/body/jhi-main/jhi-paymentreview/div[2]/div/div/div/div/div/div[2]/div[2]/div[1]/div/div/div/div/p/button")).click();
			Thread.sleep(2000);
			driver.findElement(By.xpath("//*[@id=\"couponCode\"]")).sendKeys("coupon10");
			Thread.sleep(2000);
			Log.info("Trainer Discount Coupon Entered");
			driver.findElement(By.xpath("//*[@id=\"applyCoupon\"]")).click();
			Thread.sleep(2000);	
			Log.info("Click action done on apply coupon button and Coupon applied");
			
			action.sendKeys(Keys.PAGE_DOWN).build().perform();
			Thread.sleep(2000);
			
			driver.findElement(By.xpath("/html/body/jhi-main/jhi-paymentreview/div[2]/div/div/div/div/div/div[3]/div/div[2]/div/button")).click();
			Thread.sleep(12000);
			
			SessionPayment.CreditorDebitCard.click();
			Log.info("Selected CreditorDebitCard Tab");
			Thread.sleep(6000);

			SessionPayment.EnterCardNumber.sendKeys("42424242424242424242424");
			Log.info("Enter Valid Credit/DebitCard Number");
     		Thread.sleep(5000);
     		
     		SessionPayment.ClickonPayButton.click();
     		Thread.sleep(18000);
    		Log.info("Clicked on PayNow Button");
     		
//     		SessionPayment.ClickonPreviewSessionButton.click();
//     		Thread.sleep(9000);
//     		
//			singlesession.DeleteWorkoutSession.click();
//			Thread.sleep(1000);
//			
//			singlesession.ConfirmDeleteWorkoutSession.click();
//			Thread.sleep(10000);
//			
    		Assert.assertEquals("Workout Sessions" ,"Workout Sessions");
    		Log.info("Successfully verified Page Title.");	
    		Log.info("Session Payment Successfully done with Credit/Debit Card");
			
		}	

	public static void PaymentwithPayPal(WebDriver driver) throws Exception{
		CreateSingleSessionPage singlesession = new CreateSingleSessionPage(driver);
		SessionsPaymentPages SessionPayment = new SessionsPaymentPages(driver);
		JavascriptExecutor js = (JavascriptExecutor) driver; 
		
		    singlesession.CreateScheduleButton.isDisplayed();
			singlesession.CreateScheduleButton.isEnabled();
			singlesession.CreateScheduleButton.click();
		    Thread.sleep(2000);
			Log.info("Click action performed on CreateScheduleButton.");
			
			singlesession.NameoftheWorkoutSession.isDisplayed();
			singlesession.NameoftheWorkoutSession.clear();
			singlesession.NameoftheWorkoutSession.sendKeys(CreateScheduleshelper.singleworkoutsession());
		    Thread.sleep(1000);
			Log.info("Entered name of the workoutsession");
			
			singlesession.TypeofWorkout.isDisplayed();
			singlesession.TypeofWorkout.click();
			singlesession.TypeofWorkout.sendKeys(CreateScheduleshelper.TypeofWorkout());
			Log.info("Session TypeofWorkout is selected");
			
			singlesession.ClickonGroupSize.click();
			singlesession.GroupSize.sendKeys(CreateScheduleshelper.GroupSession());
			Log.info("Group Session size is selected");
			Thread.sleep(1000);
			
			singlesession.WorkoutActivity.isDisplayed();
			singlesession.WorkoutActivity.click();
			
			singlesession.WorkoutAcvtivitySelection.clear();
			singlesession.WorkoutAcvtivitySelection.sendKeys(CreateScheduleshelper.WorkoutActivity());
			/*Thread.sleep(3000);*/
			
			singlesession.ClickonWorkoutAcvtivity.click();
		    Thread.sleep(1000);
			Log.info("Session WorkoutActivity is selected");
			
			singlesession.TrainingLevel.isDisplayed();
			singlesession.TrainingLevel.click();
			singlesession.TrainingLevel.sendKeys(CreateScheduleshelper.TrainingLevel());
		    Thread.sleep(1000);
			Log.info("Session TrainingLevel is selected");
			
			singlesession.SessionDate.isDisplayed();
			singlesession.SessionDate.click();
			singlesession.SessionDate.sendKeys(CreateScheduleshelper.AddDatestocurrentSystemDate("mm/dd/yyyy",Constant.StartDate));
			Thread.sleep(2000);
			Log.info("Session Date is selected");
			
			singlesession.SessionTime.isDisplayed();
			singlesession.SessionTime.clear();
			singlesession.SessionTime.sendKeys(CreateScheduleshelper.randomTimeGenerate("hh:mm a"));
			Log.info("Session time is selected");
			
			singlesession.SessionDuration.isDisplayed();
			singlesession.SessionDuration.click();
			singlesession.SessionDuration.sendKeys(CreateScheduleshelper.Duration("Minutes"));
			Log.info("Session Duration is selected");
			Thread.sleep(3000);
			
			Actions action = new Actions(driver);
			action.sendKeys(Keys.PAGE_DOWN).build().perform();
			
			SessionPayment.SessionPrice.clear();
			Thread.sleep(1000);
			SessionPayment.SessionPrice.sendKeys("0");	
			Log.info("Session Price Entered");
			
			Thread.sleep(1000);
			singlesession.PublishButton.isDisplayed();
			singlesession.PublishButton.isEnabled();
			singlesession.PublishButton.click();
		    Thread.sleep(8000);
			Log.info("Clicked on Publish Button.");
			
			driver.findElement(By.xpath("/html/body/jhi-main/jhi-paymentreview/div[2]/div/div/div/div/div/div[2]/div[2]/div[1]/div/div/div/div/p/button")).click();
			Thread.sleep(2000);
			driver.findElement(By.xpath("//*[@id=\"couponCode\"]")).sendKeys("AutomationCode500");
			Thread.sleep(2000);
			Log.info("TRainer Discount Coupon Entered");
			driver.findElement(By.xpath("//*[@id=\"applyCoupon\"]")).click();
			Thread.sleep(2000);	
			Log.info("Click action done on apply coupon button and Coupon applied");
			
			//action.sendKeys(Keys.PAGE_DOWN).build().perform();
			//Thread.sleep(2000);
			
			driver.findElement(By.xpath("/html/body/jhi-main/jhi-paymentreview/div[2]/div/div/div/div/div/div[3]/div/div[2]/div/button")).click();
			Thread.sleep(8000);
			
			SessionPayment.ClickonPayPalTab.click();
			Log.info("Clicked on PayPal tab ");
			Thread.sleep(3000);
			
		//	action.sendKeys(Keys.PAGE_DOWN).build().perform();
		//	Thread.sleep(2000);
			SessionPayment.ContinuetoPayPalButton.click();
			Thread.sleep(8000);
//			
//			SessionPayment.PayPalLoginButton.click();
//			Thread.sleep(5000);
			
			SessionPayment.EnterPayPalEmailID.sendKeys("vijaykumarkalpagur@gmail.com");
			SessionPayment.ClickOnNextButton.click();
			Thread.sleep(3000);
			Log.info("Entered PayPal valid Email ID");
			
			SessionPayment.EnterPayPalPassword.sendKeys("Testing@123");
			Log.info("Entered PayPal valid Password");
			Thread.sleep(3000);

			SessionPayment.ClickOnLoginButton.click();
			Thread.sleep(12000);
			//Log.info("Click on PayPal Account Login Button");
			
			//action.sendKeys(Keys.PAGE_DOWN).build().perform();
			//Thread.sleep(5000);
			Thread.sleep(15000);
			js.executeScript("window.scrollBy(0,10000)");
			
			Thread.sleep(3000);
			
			SessionPayment.ClickonconfirmButton.click();
     		Thread.sleep(18000);
     		
     		js.executeScript("window.scrollBy(0,10000)");
			
			Thread.sleep(10000);
			Assert.assertTrue(driver.findElement(By.xpath("/html/body/div[1]/div[2]/div[2]/div[2]/div/div/div/div/div/div/div[2]/table/tbody/tr/td[5]/span")).isDisplayed());
			Log.info("Payment has done successfully and page has verified successfully...");
			
			Thread.sleep(1500);
			Log.info("Session Payment Successfully done with PayPal Account");
			
			}
}	

