package com.trainerweb.qa.actions;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.trainerweb.qa.helpers.CreateScheduleshelper;
import com.trainerweb.qa.pages.DailySessionPage;
import com.trainerweb.qa.utilities.Constant;
import com.trainerweb.qa.utilities.Log;


public class DailySessionAction {

	static WebDriver driver;
	
public static void DailySession(WebDriver driver) throws Exception{
	
	DailySessionPage dailysession = new DailySessionPage(driver);
    
	dailysession.CreateScheduleButton.click();
	Thread.sleep(3000);
	Log.info("Click action performed on CreateScheduleButton.");
			
	dailysession.NameoftheWorkoutSession.sendKeys(CreateScheduleshelper.singleworkoutsession());
    Thread.sleep(1000);
	Log.info("Entered name of the workoutsession");
			
	//dailysession.TypeofWorkout.click();
	//dailysession.TypeofWorkout.sendKeys(CreateScheduleshelper.TypeofWorkout());
	Log.info("Session TypeofWorkout is selected");
			
//	dailysession.ClickonGroupSize.click();
//	dailysession.GroupSize.sendKeys(CreateScheduleshelper.GroupSession());
//	Log.info("Group Session size is selected");
	Thread.sleep(1000);
			
	dailysession.WorkoutActivity.click();

	dailysession.WorkoutAcvtivitySelection.sendKeys(CreateScheduleshelper.WorkoutActivity());
	Thread.sleep(3000);		
	dailysession.ClickonWorkoutAcvtivity.click();
	Thread.sleep(1000);
	Log.info("Session WorkoutActivity is selected");
			
	dailysession.TrainingLevel.click();
	dailysession.TrainingLevel.sendKeys(CreateScheduleshelper.TrainingLevel());
	Thread.sleep(1000);
	Log.info("Session TrainingLevel is selected");
	
	dailysession.RecurringSessionRadio.click();
	Thread.sleep(3000);
	
	dailysession.SessionStartDate.sendKeys(CreateScheduleshelper.AddDatestocurrentSystemDate("mm/dd/yyyy",Constant.StartDate));
	Thread.sleep(2000);
	Log.info("Session Start Date is selected");

	dailysession.DailySessionEndDate.sendKeys(CreateScheduleshelper.AddDatestocurrentSystemDate("mm/dd/yyyy",Constant.EndDate));
	Thread.sleep(2000);
	Log.info("Session End Date is selected");
	
	dailysession.SessionTime.sendKeys(CreateScheduleshelper.randomTimeGenerate("hh:mm a"));
	Log.info("Session time is selected");
	

	dailysession.SessionDuration.sendKeys(CreateScheduleshelper.Duration("Minutes"));
	Log.info("Session Duration is selected");
	Thread.sleep(3000);
	
	Actions action = new Actions(driver);
	action.sendKeys(Keys.PAGE_DOWN).build().perform();
	
	dailysession.PublishButton.click();
	Log.info("Click action performed on Publish Button.");	
	Thread.sleep(10000);
	Log.info("Click action performed on Publish Button.");
	
	dailysession.DeleteWorkoutSession.click();
	Thread.sleep(2000);
	
	dailysession.ConfirmDeleteWorkoutSession.click();
	Log.info("CustomSession Session Deleted Succesfully");
	
//	Thread.sleep(5000);
	
	//Assert.assertEquals("Workout Sessions" ,"Workout Sessions");
	//Log.info("Successfully verified Page Title.");	
	Log.info("Successfully created Daily Workout Session with valid inputs");
	}
}			

